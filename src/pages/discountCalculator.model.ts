export interface IDiscountOnItems {
    [key: number]: number;
}
export interface IProductQuantityValues {
    [key: string]: number;
}