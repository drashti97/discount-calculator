import React, { useState } from 'react'
import { Button, Card, Col, Form, InputNumber, Row } from 'antd';
import { IDiscountOnItems, IProductQuantityValues } from './discountCalculator.model';
import _ from 'lodash';

// constant values
const PRODUCT_PRICE: number = 8;
const DISCOUNT_ON_ITEMS: IDiscountOnItems = {
    2: 5,
    3: 10,
    4: 20,
    5: 25,
}

const DiscountCalculator: React.FC = () => {
    // Array of product categories
    const categories: Array<string> = ['first shirt', 'second shirt', 'third shirt', 'fourth shirt', 'fifth shirt'];

    const [lowestPrice, setLowestPrice] = useState<number>(0);
    const [form] = Form.useForm();

    const onFinish = (values: IProductQuantityValues) => {
        // Remove undefined and null values from object
        const filterValues = _.pickBy(values, function (value) {
            return !(
                value === undefined || _.isNull(value) || value <= 0
            );
        });

        // Sort object in descending order by quantity
        const sortedValues = Object.entries(filterValues).sort((a, b) => b[1] - a[1]).reduce(function (obj, item) {
            obj = { ...obj, [item[0]]: item[1] };
            return obj;
        }, {});

        // Array to store all prices of different combinations
        const prices = [];

        // Making combinations of group
        for (let i = categories.length; i > 1; i--) {
            let remainingQty = sortedValues;
            let price = 0;

            // Making combinations till all remaining items will be 0
            while (Object.values(remainingQty).length > 0 && Object.values(remainingQty).reduce((a: any, b: any) => {
                return a + b;
            }) !== 0) {
                let grpQty = 0; // Constant to store quantity of combination group
                for (let j = 0; j < categories.length; j++) {
                    const currQty: any = Object.values(remainingQty)[j]
                    if (grpQty < i) {
                        if (currQty > 0) {
                            remainingQty = {
                                ...remainingQty,
                                [Object.keys(remainingQty)[j]]: currQty - 1
                            }
                            grpQty++;
                        }
                    }
                }
                // Calculate price by combination group
                price = grpQty > 1 ? price + grpQty * (PRODUCT_PRICE - ((PRODUCT_PRICE * DISCOUNT_ON_ITEMS[grpQty]) / 100)) : price + PRODUCT_PRICE;
            }
            // Store price in array
            prices.push(price)
        }
        // Find and store lowest price from array
        setLowestPrice(prices.sort()[0]);
    }

    return (
        <div>
            <Row justify="center" style={{ marginBottom: '10px' }}>
                <Col span={2}><b>Category</b></Col>
                <Col span={2}><b>Qty</b></Col>
            </Row>

            <Form
                form={form}
                name="discountCalc"
                onFinish={onFinish}
            >
                {categories.length > 0 && categories.map((item, index) => (
                    <Row justify="center" key={index}>
                        <Col span={2}>{_.capitalize(item)}</Col>
                        <Col span={2}>
                            <Form.Item name={item} rules={[{ type: 'integer' }]}>
                                <InputNumber min={1} />
                            </Form.Item>
                        </Col>
                    </Row>
                ))}
                <Row justify="center">
                    <Col span={2}>
                        <Button
                            type="primary"
                            htmlType="submit"
                        >
                            Calculate
                        </Button>
                    </Col>
                    <Col span={2}>
                        <Button onClick={() => { form.resetFields(); setLowestPrice(0) }}>Reset</Button>
                    </Col>
                </Row>
            </Form>
            {lowestPrice > 0 && (
                <Card style={{ marginTop: '10px' }}>
                    <span style={{ fontWeight: 'bold' }}>${lowestPrice.toFixed(2)}</span> is the price with the biggest discount.
                </Card>
            )}
        </div>
    )
}
export default DiscountCalculator;