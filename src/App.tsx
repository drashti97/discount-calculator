import React from 'react';
import './App.css';
import { Header } from 'antd/lib/layout/layout';
import DiscountCalculator from './pages';

function App() {
  return (
    <div className="App">
      <Header className="header">Discount Calculator</Header>
      <DiscountCalculator />
    </div>
  );
}

export default App;
